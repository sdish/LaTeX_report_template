#!/usr/bin/python

import os
import sys
from optparse import OptionParser


#=============================================================================================================
def compile_tex(name):

	latex = "pdflatex -jobname "+name+" report.tex"

	os.system(latex)
	os.system("bibtex report.aux")
	os.system(latex)
	os.system(latex)

#=============================================================================================================
def remove_tex():
	os.system("rm *blg *bbl *toc *out *log *pdf *aux *lof *lot")
	print "removed non-source files"

#=============================================================================================================



parser = OptionParser("usage: %prog [options] [arguments]. For help run with flag -h")
parser.add_option("-c", "--create-pdf", 	help = "create report pdf",       	dest = "make_pdf",		default = False,	action = "store_true") 
parser.add_option("-n", "--name",       	help = "add a custom pdf name",	    dest = "file_name",		default = "report")
parser.add_option("-x", "--remove-pdf", 	help = "remove non-source files",	dest = "remove_pdf",	default = False,	action = "store_true") 

(options, args) = parser.parse_args()

if (not args and not options):
	os.system("./make.py -h")

if options.make_pdf and options.remove_pdf:
	print "Must either make or remove pdf"

if options.make_pdf:
	compile_tex(options.file_name)
	

if options.remove_pdf:
	remove_tex()

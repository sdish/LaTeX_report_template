# LaTeX_report_template
A basic template I use for my reports - because there aren't enough of these floating around!

!run `chmod +x make.py' if the below lines do not work!

## Help with compilation

##### Basic compilation
	./make.py -c

##### Remove non-source files
	./make.py -x

##### Help
	./make.py -h

##### Custom output pdf filename
	./make.py -c -n <FILENAME>

## Report class
This report uses a custom class "myReport.cls" to define a few useful things. I may add to it over time....
